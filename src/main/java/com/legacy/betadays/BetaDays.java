package com.legacy.betadays;

import net.minecraft.item.Item;
import net.minecraft.item.Items;
import net.minecraft.util.ResourceLocation;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.ModLoadingContext;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.ObfuscationReflectionHelper;
import net.minecraftforge.fml.config.ModConfig;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import net.minecraftforge.registries.ForgeRegistries;

@Mod(BetaDays.MODID)
public class BetaDays
{

	public BetaDays()
	{
		ModLoadingContext.get().registerConfig(ModConfig.Type.CLIENT, BetaDaysConfig.CLIENT_SPEC);
		ModLoadingContext.get().registerConfig(ModConfig.Type.COMMON, BetaDaysConfig.SERVER_SPEC);

		FMLJavaModLoadingContext.get().getModEventBus().addListener(this::commonInit);
	}

	public static final String NAME = "Beta Days";
	public static final String MODID = "beta_days";

	public static void registerEvent(Object obj)
	{
		MinecraftForge.EVENT_BUS.register(obj);
	}

	public static ResourceLocation locate(String key)
	{
		return new ResourceLocation(MODID, key);
	}

	public static String find(String key)
	{
		return MODID + ":" + key;
	}

	@SubscribeEvent
	public void commonInit(FMLCommonSetupEvent event)
	{
		BetaDays.registerEvent(new BetaPlayerEvents());

		for (Item item : ForgeRegistries.ITEMS.getValues())
		{
			if (BetaDaysConfig.disableFoodStacking && item.isFood())
			{
				ObfuscationReflectionHelper.setPrivateValue(Item.class, item, item == Items.COOKIE ? 8 : 1, "field_77777_bU");
			}
		}
	}
}