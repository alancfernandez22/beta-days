package com.legacy.betadays.client.gui;

import com.mojang.blaze3d.matrix.MatrixStack;

import net.minecraft.client.gui.chat.NarratorChatListener;
import net.minecraft.client.gui.screen.Screen;
import net.minecraft.client.resources.I18n;

public class LoadingDimensionsScreen extends Screen
{
	public boolean nether;

	public boolean leaving;

	public LoadingDimensionsScreen(boolean isNether, boolean leaving)
	{
		super(NarratorChatListener.EMPTY);
		this.nether = isNether;
		this.leaving = leaving;
	}

	@Override
	public boolean shouldCloseOnEsc()
	{
		return false;
	}

	@Override
	public void render(MatrixStack stack, int mouseX, int mouseY, float partialTicks)
	{
		this.renderDirtBackground(0);
		drawCenteredString(stack, this.font, I18n.format("menu.generatingTerrain"), this.width / 2, this.height / 2 - 30, 16777215);

		if (!leaving)
		{
			if (nether)
			{
				drawCenteredString(stack, this.font, I18n.format("beta.menu.enterNether"), this.width / 2, this.height / 2 - 50, 16777215);
			}
			else
			{
				drawCenteredString(stack, this.font, I18n.format("beta.menu.enterEnd"), this.width / 2, this.height / 2 - 50, 16777215);
			}
		}
		else
		{
			if (nether)
			{
				drawCenteredString(stack, this.font, I18n.format("beta.menu.leaveNether"), this.width / 2, this.height / 2 - 50, 16777215);
			}
			else
			{
				drawCenteredString(stack, this.font, I18n.format("beta.menu.leaveEnd"), this.width / 2, this.height / 2 - 50, 16777215);
			}
		}

		super.render(stack, mouseX, mouseY, partialTicks);
	}

	@Override
	public boolean isPauseScreen()
	{
		return false;
	}
}