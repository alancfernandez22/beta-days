package com.legacy.betadays.client;

import com.legacy.betadays.BetaDaysConfig;
import com.legacy.betadays.client.gui.ClassicMainMenuScreen;
import com.legacy.betadays.client.gui.LoadingDimensionsScreen;
import com.legacy.betadays.client.keybindings.BetaDaysKeyRegistry;
import com.mojang.blaze3d.platform.GlStateManager;
import com.mojang.blaze3d.systems.RenderSystem;

import net.minecraft.client.Minecraft;
import net.minecraft.client.gui.screen.DownloadTerrainScreen;
import net.minecraft.client.gui.screen.MainMenuScreen;
import net.minecraft.client.settings.KeyBinding;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.ResourceLocation;
import net.minecraft.util.SoundEvents;
import net.minecraft.world.World;
import net.minecraftforge.api.distmarker.Dist;
import net.minecraftforge.api.distmarker.OnlyIn;
import net.minecraftforge.client.event.EntityViewRenderEvent;
import net.minecraftforge.client.event.GuiOpenEvent;
import net.minecraftforge.client.event.sound.PlaySoundEvent;
import net.minecraftforge.event.entity.living.LivingEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;

@OnlyIn(Dist.CLIENT)
public class BetaClientEvent
{
	private final Minecraft mc = Minecraft.getInstance();
	private int prevDimension = 0;

	@SubscribeEvent
	public void onOpenGui(GuiOpenEvent event)
	{
		if (mc.player != null && event.getGui() instanceof DownloadTerrainScreen && BetaDaysConfig.customDimensionMessages)
		{
			LoadingDimensionsScreen guiEnterEnd = new LoadingDimensionsScreen(false, false);
			LoadingDimensionsScreen guiEnterNether = new LoadingDimensionsScreen(true, false);

			LoadingDimensionsScreen guiLeaveEnd = new LoadingDimensionsScreen(false, true);
			LoadingDimensionsScreen guiLeaveNether = new LoadingDimensionsScreen(true, true);

			if (mc.player.world.getDimensionKey() != World.OVERWORLD) // not overworld
			{
				if (mc.player.world.getDimensionKey() == World.THE_NETHER) // is nether
				{
					event.setGui(guiLeaveNether);
					prevDimension = -1;
				}

				if (mc.player.world.getDimensionKey() == World.THE_END)
				{
					event.setGui(guiLeaveEnd);
					prevDimension = 1;
				}
			}
			else if (mc.player.world.getDimensionKey() == World.OVERWORLD) // is overworld
			{
				if (prevDimension == -1)
				{
					event.setGui(guiEnterNether);
					prevDimension = 0;
				}

				if (prevDimension == 1)
				{
					event.setGui(guiEnterEnd);
					prevDimension = 0;
				}
			}
		}

		if (BetaDaysConfig.enableClassicMenu && event.getGui() != null && event.getGui().getClass() == MainMenuScreen.class && !this.mc.isDemo())
		{
			event.setGui(new ClassicMainMenuScreen(false));
		}
	}

	@SubscribeEvent
	public void onPlayerUpdate(LivingEvent.LivingUpdateEvent event)
	{
		if (event.getEntityLiving() instanceof PlayerEntity)
		{
			if (BetaDaysConfig.disableSprinting && !((PlayerEntity) event.getEntityLiving()).isCreative())
			{
				KeyBinding.setKeyBindState(mc.gameSettings.keyBindSprint.getKey(), false);
			}
		}
	}

	@SuppressWarnings("deprecation")
	@SubscribeEvent
	public void onFogRender(EntityViewRenderEvent.RenderFogEvent event)
	{
		World world = mc.world;
		PlayerEntity player = mc.player;

		if (world == null || !world.isRemote)
		{
			return;
		}

		if (BetaDaysConfig.disableNetherFog && mc.player.world.getDimensionKey() == World.THE_NETHER) // is nether
		{
			RenderSystem.fogMode(GlStateManager.FogMode.EXP);
			RenderSystem.fogDensity(0.1F * 0.0F);
			RenderSystem.enableFog();
		}
	}

	@SubscribeEvent
	public void onSoundPlayed(PlaySoundEvent event)
	{
		ResourceLocation sound = event.getSound().getSoundLocation();

		if (BetaDaysConfig.disableCombatSounds)
		{
			if (sound == SoundEvents.ENTITY_PLAYER_ATTACK_NODAMAGE.getName() || sound == SoundEvents.ENTITY_PLAYER_ATTACK_WEAK.getName() || sound == SoundEvents.ENTITY_PLAYER_ATTACK_STRONG.getName() || sound == SoundEvents.ENTITY_PLAYER_ATTACK_CRIT.getName() || sound == SoundEvents.ENTITY_PLAYER_ATTACK_SWEEP.getName() || sound == SoundEvents.ENTITY_PLAYER_ATTACK_KNOCKBACK.getName())
			{
				event.setResultSound(null);
			}
		}
	}

	@SubscribeEvent
	public void onKeyEvent(BetaDaysKeyRegistry.BetaKeyEvent event)
	{
		if (event.getKeyBinding() == BetaDaysClient.TOGGLE_FOG_BIND && event.getAction() == BetaDaysKeyRegistry.EnumKeyAction.PRESSED)
		{
			if (mc.world != null)
			{
				final int maxFogDistance = BetaDaysConfig.toggleFogMaxDistance;
				final int interval = 2;
				int renderDistance = (mc.gameSettings.renderDistanceChunks + interval) % maxFogDistance;
				if (renderDistance <= 0 || renderDistance >= BetaDaysConfig.toggleFogMaxDistance)
					renderDistance = maxFogDistance;
				if (mc.gameSettings.renderDistanceChunks != renderDistance)
				{
					mc.gameSettings.renderDistanceChunks = renderDistance;
					mc.worldRenderer.setDisplayListEntitiesDirty();
				}
			}
		}
	}
}