package com.legacy.betadays.mixin;

import org.spongepowered.asm.mixin.Mixin;
import org.spongepowered.asm.mixin.injection.At;
import org.spongepowered.asm.mixin.injection.Inject;
import org.spongepowered.asm.mixin.injection.callback.CallbackInfo;

import com.legacy.betadays.BetaDaysConfig;

import net.minecraft.enchantment.EnchantmentHelper;
import net.minecraft.entity.Entity;
import net.minecraft.entity.player.PlayerEntity;
import net.minecraft.util.SoundEvents;

@Mixin(PlayerEntity.class)
public class PlayerEntityMixin
{
	@Inject(at = @At(value = "INVOKE_ASSIGN", target = "net/minecraft/util/math/MathHelper.sin(F)F"), method = "attackTargetEntityWithCurrentItem(Lnet/minecraft/entity/Entity;)V", cancellable = true)
	private void attackTargetEntityWithCurrentItem(Entity targetEntity, CallbackInfo callback)
	{
		PlayerEntity player = (PlayerEntity) (Object) this;

		if (BetaDaysConfig.disableCombatSweep && EnchantmentHelper.getSweepingDamageRatio(player) <= 0.0F)
		{
			if (!BetaDaysConfig.disableCombatSounds)
				player.world.playSound((PlayerEntity) null, player.getPosX(), player.getPosY(), player.getPosZ(), SoundEvents.ENTITY_PLAYER_ATTACK_SWEEP, player.getSoundCategory(), 1.0F, 1.0F);

			callback.cancel();
		}
	}

	@Inject(at = @At("HEAD"), method = "spawnSweepParticles()V", cancellable = true)
	private void spawnSweepParticles(CallbackInfo callback)
	{
		PlayerEntity player = (PlayerEntity) (Object) this;

		if (BetaDaysConfig.disableCombatSweep && EnchantmentHelper.getSweepingDamageRatio(player) <= 0.0F)
			callback.cancel();
	}
}
